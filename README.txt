-- SUMMARY --

The MathJax CDN module includes resources from the MathJax Content Delivery
Network (http://www.mathjax.org/) into all pages in a Drupal site.

-- REQUIREMENTS --

None.
